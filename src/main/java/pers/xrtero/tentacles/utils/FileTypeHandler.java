package pers.xrtero.tentacles.utils;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;

import javax.xml.crypto.Data;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 从URL中解析文件名
 */
public class FileTypeHandler {

    private static MimeTypes all = MimeTypes.getDefaultMimeTypes();

    public static String getFileName(String path){
        String[] s = path.split("/");

        return s[s.length-1];
    }



    public static String parser(String contentType,String path){
        MimeType mimeType = null;
        try {
            mimeType = all.forName(contentType);
        } catch (MimeTypeException e) {
            e.printStackTrace();
        }
        if(contentType.equals("application/octet-stream")){
            return FileTypeHandler.getFileName(path);
        }
        LocalDateTime localDateTime = LocalDateTime.now();
        return localDateTime.getNano()+mimeType.getExtension();

    }





}
