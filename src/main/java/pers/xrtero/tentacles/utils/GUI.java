package pers.xrtero.tentacles.utils;


/**
 * 文件与文件名的对应关系
 */

public class GUI {
    public static String NEW_STAGE_PATH = "/ui/new.fxml";
    public static String SETTINGS_STAGE_PATH = "/ui/settings.fxml";
    public static String DOWNLOADING_STAGE_PATH = "/ui/downloading.fxml";
    public static String PROPERTY_STAGE_PATH = "/ui/property.fxml";
    public static String MAIN_STAGE_PATH = "/ui/main.fxml";

    public static String NEW_STAGE_NAME = "new";
    public static String SETTINGS_STAGE_NAME = "settings";
    public static String DOWNLOADING_STAGE_NAME = "downloading";
    public static String PROPERTY_STAGE_NAME = "property";
    public static String MAIN_STAGE_NAME = "main";
}
