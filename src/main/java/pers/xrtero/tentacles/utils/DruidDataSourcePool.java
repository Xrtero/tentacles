package pers.xrtero.tentacles.utils;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 构造连接池，可从连接池获取连接
 * @author Xrtero
 */
public class DruidDataSourcePool {
    private static DataSource dataSource = null;
    static{
        try {
            InputStream fis = DruidDataSourcePool.class.getClassLoader().getResourceAsStream("config/druid.properties");
            Properties properties = new Properties();
            properties.load(fis);
            dataSource = DruidDataSourceFactory.createDataSource(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public static Connection getConnection(){
        try {
           return dataSource.getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

}
