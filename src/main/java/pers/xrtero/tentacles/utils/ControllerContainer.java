package pers.xrtero.tentacles.utils;


import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pers.xrtero.tentacles.controller.Controller;
import pers.xrtero.tentacles.utils.GUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Description 存储所有Stage的Map
 * @author Xrtero
 */
public class ControllerContainer {

    private static HashMap<String, List> container = new HashMap<>();


    public static void add(String name, String localPath, Controller controller){
        try {
            FXMLLoader loader = new FXMLLoader(GUI.class.getResource(localPath));
            loader.setController(controller);
            Pane load = loader.load();
            Scene scene = new Scene(load,load.getMinWidth(), load.getMinHeight());
            Stage stage = new Stage();
            stage.setScene(scene);
            List list = new ArrayList();
            list.add(stage);
            list.add(loader.getController());
            stage.getIcons().add(new Image(ControllerContainer.class.getResourceAsStream("/icon/mascot.png")));
            stage.setResizable(false);
            container.put(name,list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Controller getControllerByName(String name){
        List list = container.get(name);
        return (Controller) list.get(1);
    }

    public static Stage getStageByName(String name){
        List list = container.get(name);
        return (Stage) list.get(0);
    }


    public static boolean remove(String name){
       if(container.containsKey(name)){
           container.remove(name);
           return true;
       }else{
           return false;
       }
    }







}

