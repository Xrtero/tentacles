package pers.xrtero.tentacles.utils;

import javafx.scene.control.Alert;

/**
 * 提示与警告框构造类
 * @author Xrtero
 */
public class AlertUtils {


    public static void informationBuild(String title,String alertContent){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle(title);
        alert.setContentText(alertContent);
        alert.showAndWait();
    }

    public static void warmingBuild(String title,String alertContent){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(null);
        alert.setTitle(title);
        alert.setContentText(alertContent);
        alert.showAndWait();
    }
}
