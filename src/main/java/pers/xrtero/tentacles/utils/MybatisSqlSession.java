package pers.xrtero.tentacles.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;

/**
 * mybatis session
 * @author Xrtero
 */
public class MybatisSqlSession {

    private static  SqlSessionFactory build = null;
    static {
        String resource = "config/mybatis-conf.xml";
        DataSource dataSource;
        try {
            InputStream resourceAsStream = Resources.getResourceAsStream(resource);
            build = new SqlSessionFactoryBuilder().build(resourceAsStream);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static SqlSession getSession(){
        return build.openSession(DruidDataSourcePool.getConnection());
    }
}
