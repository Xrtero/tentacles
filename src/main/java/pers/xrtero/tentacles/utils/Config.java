package pers.xrtero.tentacles.utils;

import pers.xrtero.tentacles.service.Downloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * 读取并设置配置文件
 * @author Xrtero
 */
public class Config {

    private static String configPath = "/config/config.properties";
    public static String localFileSavePath;
    public static Integer threadSize;
    public static Integer timeOut;
    private static Properties properties  = new Properties();;

    static{
        loadConfig();
    }

    public static void loadConfig(){
        try {
            properties.load(Downloader.class.getResourceAsStream(configPath));
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        threadSize = Integer.parseInt(properties.getProperty("threadSize"));
        localFileSavePath = properties.getProperty("localFileSavePath");
        timeOut = Integer.parseInt(properties.getProperty("timeOut"));
    }


    public static void setLocalFileSavePath(String path){
        localFileSavePath = path;
        properties.setProperty("localFileSavePath",path);
    }
    public static void setThreadSize(Integer size){
        threadSize = size;
        properties.setProperty("threadSize",size.toString());
    }
    public static void setTimeOut(Integer time){
        timeOut = time;
        properties.setProperty("timeOut",timeOut.toString());
    }

    public static void saveConfig(){
        FileOutputStream fos = null;
        try {
            URL url  = Config.class.getResource(configPath);
            String file = url.getFile();
            fos = new FileOutputStream(file);
            properties.store(fos,null);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }




}
