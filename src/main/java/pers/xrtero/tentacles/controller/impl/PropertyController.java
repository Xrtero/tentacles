package pers.xrtero.tentacles.controller.impl;


import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.ibatis.session.SqlSession;
import pers.xrtero.tentacles.controller.Controller;
import pers.xrtero.tentacles.dao.FileInfoDao;
import pers.xrtero.tentacles.entity.FileInfo;
import pers.xrtero.tentacles.enums.FileStatusEnum.FileStatusEnum;
import pers.xrtero.tentacles.service.Downloader;
import pers.xrtero.tentacles.utils.AlertUtils;
import pers.xrtero.tentacles.utils.ControllerContainer;
import pers.xrtero.tentacles.utils.GUI;
import pers.xrtero.tentacles.utils.MybatisSqlSession;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * 任务属性UI
 * @author Xrtero
 */
public class PropertyController implements Controller {

    @FXML private Label url;
    @FXML private Label fileSize;
    @FXML private Label downloaded;
    @FXML private Label status;
    @FXML private TextField fileName;

    @FXML private Button commit;
    @FXML private Button cancel;
    @FXML private Button resume;
    private Downloader downloader = null;


    private FileInfo fileInfo;


    public PropertyController() {
    }

    public PropertyController(FileInfo fileInfo) {
        this.fileInfo = fileInfo;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fileName.setOnAction((event)->{
            String text = fileName.getText();
            fileName.setText(text);
        });

        commit.setOnAction(event -> {
            String name = fileName.getText();
            fileInfo.setFileName(name);
            SqlSession session = MybatisSqlSession.getSession();
            FileInfoDao mapper = session.getMapper(FileInfoDao.class);
            mapper.updateName(fileInfo);
            session.close();

            Platform.runLater(()->{
                fileName.setText(fileInfo.getFileName());
            });
            remove();
        });

        cancel.setOnAction((event)->{
            remove();
        });

        resume.setOnAction((event -> {
            try{
                downloader =  new Downloader(fileInfo.getId());
                ControllerContainer.add(GUI.DOWNLOADING_STAGE_NAME,GUI.DOWNLOADING_STAGE_PATH,new DownloadingController(downloader));
                ControllerContainer.getStageByName(GUI.DOWNLOADING_STAGE_NAME).show();
                ControllerContainer.getStageByName(GUI.PROPERTY_STAGE_NAME).close();
                ControllerContainer.remove(GUI.PROPERTY_STAGE_NAME);
            } catch (InstantiationException e) {
                Platform.runLater(()->{
                    AlertUtils.informationBuild("通知","不支持断点续传");
                });
            }
        }));


    }

    private void succeed() {
        Platform.runLater(()->{
            AlertUtils.informationBuild("下载完成",downloader.getFileInfo().getFileName()+"下载完成!");
            downloader.saveOnSuccess();
            ((Stage) ControllerContainer.getStageByName(GUI.DOWNLOADING_STAGE_NAME)).close();
            ControllerContainer.remove(GUI.DOWNLOADING_STAGE_NAME);
        });

    }

    @Override
    public void refresh() {
        Platform.runLater(()->{
            url.setText(fileInfo.getUrl());
            fileSize.setText(Downloader.format(fileInfo.getFileSize()));
            status.setText(fileInfo.getFileStatus().toString());
            fileName.setText(fileInfo.getFileName());
            if(fileInfo.getFileStatus() != FileStatusEnum.pause){
                resume.setVisible(false);
            }
        });

    }


    public void remove(){
        ((Stage) ControllerContainer.getStageByName(GUI.PROPERTY_STAGE_NAME)).close();
        ControllerContainer.remove(GUI.PROPERTY_STAGE_NAME);
    }

    public FileInfo getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(FileInfo fileInfo) {
        this.fileInfo = fileInfo;
    }


}
