package pers.xrtero.tentacles.controller.impl;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import pers.xrtero.tentacles.controller.Controller;
import pers.xrtero.tentacles.entity.FileInfo;
import pers.xrtero.tentacles.enums.ErrorCode;
import pers.xrtero.tentacles.exception.URLException;
import pers.xrtero.tentacles.service.Downloader;
import pers.xrtero.tentacles.utils.AlertUtils;
import pers.xrtero.tentacles.utils.ControllerContainer;
import pers.xrtero.tentacles.utils.Config;
import pers.xrtero.tentacles.utils.GUI;
import sun.applet.Main;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * 新建任务UI
 * @author Xrtero
 */
public class NewController implements Controller {


    private HttpURLConnection conn = null;
    @FXML
    private TextField urlTextField;
    @FXML
    private Button downloadButton;
    @FXML
    private Button cancelButton;
    @FXML
    private Pane newPane;

    private static String urlContent;


    public NewController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        cancelButton.setOnAction(event -> {
            ((Stage)cancelButton.getScene().getWindow()).close();
        });


        downloadButton.setOnAction(event -> {
            try {
                urlContent = urlTextField.getText();
                URL url = new URL(urlContent);
                conn = (HttpURLConnection) url.openConnection();
                conn.setConnectTimeout(Config.timeOut);
                conn.setReadTimeout(Config.timeOut);
                int responseCode = conn.getResponseCode();
                if (urlContent.isEmpty()){
                    AlertUtils.warmingBuild("输入错误","输入为空,请重新输入!");
                }else if(responseCode!=206&&responseCode!=200){
                    try {
                        throw new URLException(ErrorCode.URL_ERROR);
                    } catch (URLException e) {
                        AlertUtils.warmingBuild("URL错误","不支持此URL");
                    }
                } else{
                    ControllerContainer.add(GUI.DOWNLOADING_STAGE_NAME,GUI.DOWNLOADING_STAGE_PATH,new DownloadingController(new Downloader(conn)));
                    ((Stage) ControllerContainer.getStageByName(GUI.DOWNLOADING_STAGE_NAME)).show();

                    ((Stage)cancelButton.getScene().getWindow()).close();
                }
            } catch (IOException e) {
                AlertUtils.informationBuild("超时","连接超时请稍后再试");
            }
        });



    }

    public static String getUrlContent() {
        return urlContent;
    }

    public TextField getUrlTextField() {
        return urlTextField;
    }

    public Button getDownloadButton() {
        return downloadButton;
    }

    public Button getCancelButton() {
        return cancelButton;
    }

    public Pane getNewPane() {
        return newPane;
    }

    public HttpURLConnection getConn() {
        return conn;
    }

    @Override
    public void refresh() {

    }
}
