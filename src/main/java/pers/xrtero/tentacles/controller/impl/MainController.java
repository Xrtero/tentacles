package pers.xrtero.tentacles.controller.impl;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import org.apache.ibatis.session.SqlSession;
import pers.xrtero.tentacles.controller.Controller;
import pers.xrtero.tentacles.dao.FileInfoDao;
import pers.xrtero.tentacles.entity.FileInfo;
import pers.xrtero.tentacles.enums.FileStatusEnum.FileStatusEnum;
import pers.xrtero.tentacles.service.Downloader;
import pers.xrtero.tentacles.utils.ControllerContainer;
import pers.xrtero.tentacles.utils.GUI;
import pers.xrtero.tentacles.utils.MybatisSqlSession;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;


/**
 * 主界面UI
 * @author
 */
public class MainController implements Controller {

    @FXML private Button newButton;
    @FXML private Button settingButton;
    @FXML private Button deleteButton;
    @FXML private Button resumeButton;
    @FXML private Button downloadAgainButton;
    @FXML private Button stopButton;

    @FXML
    private TableView table;

    @FXML
    private TableColumn<FileInfo,Integer> tableId;
    @FXML
    private TableColumn<FileInfo,String> tableName;
    @FXML
    private TableColumn<FileInfo,Integer> tableSize;
    @FXML
    private TableColumn<FileInfo,String> tableStatus;
    @FXML
    private TableColumn<FileInfo, Date> tableCreationTime;


    @FXML
    private MenuItem newMenuItem;
    @FXML
    private MenuItem settingMenuItem;
    @FXML
    private MenuItem exitMenuItem;

    @FXML
    private MenuItem deleteMenuItem;
    @FXML
    private MenuItem renameMenuItem;
    @FXML
    private MenuItem resumeMenuItem;
    @FXML
    private MenuItem stopMenuItem;

    @FXML
    private MenuItem aboutMenuItem;




    List<FileInfo> data = null;
    public MainController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initTable();
    }


    /**
     * @Description 打开设置
     */
    @FXML public void openSettings()  {
        ControllerContainer.add(GUI.SETTINGS_STAGE_NAME,GUI.SETTINGS_STAGE_PATH,new SettingsController());
        ((Stage) ControllerContainer.getStageByName(GUI.SETTINGS_STAGE_NAME)).setOnCloseRequest(event -> {
            //当关闭时
        });
        ((Stage) ControllerContainer.getStageByName(GUI.SETTINGS_STAGE_NAME)).show();
    }


    @FXML public void exit(){
        System.exit(0);
    }

    /**
     * @Descritpion 新建任务
     */
    @FXML
    public void newProject(){
        ControllerContainer.add(GUI.NEW_STAGE_NAME,GUI.NEW_STAGE_PATH,new NewController());
        ((Stage) ControllerContainer.getStageByName(GUI.NEW_STAGE_NAME)).show();
        table.refresh();
    }



    private void initTable(){

        //还未写的功能,将按钮关闭
        deleteButton.setDisable(true);
        resumeButton.setDisable(true);
        downloadAgainButton.setDisable(true);
        stopButton.setDisable(true);


        SqlSession session = MybatisSqlSession.getSession();
        FileInfoDao mapper = session.getMapper(FileInfoDao.class);
        data = mapper.getAll();
        ObservableList observableList = new ObservableListWrapper(data);
        tableId.setCellValueFactory(
                new PropertyValueFactory<>("id")
        );
        tableName.setCellValueFactory(
                new PropertyValueFactory<>("fileName")
        );
        tableSize.setCellValueFactory(
                new PropertyValueFactory<>("fileSize")
        );
        tableStatus.setCellValueFactory(
                new PropertyValueFactory<>("fileStatus")
        );
        tableCreationTime.setCellValueFactory(
                new PropertyValueFactory<>("fileCreationTime")
        );

        table.setRowFactory((Callback<TableView, TableRow>) param -> {
            TableRow<FileInfo> row = new TableRow<>();
            ContextMenu contextMenu = new ContextMenu();
            MenuItem delete = new MenuItem("删除");
            MenuItem newFile = new MenuItem("新建");
            MenuItem attribute = new MenuItem("属性");
            MenuItem fresh = new MenuItem("刷新");

            row.setOnMouseClicked(event -> {
                if(event.getClickCount()==2&&!row.isEmpty()){
                    FileInfo item = row.getItem();
                    attribute(item);
                }
                table.refresh();
            });
            fresh.setOnAction(event -> {
                table.refresh();
            });
            delete.setOnAction(event -> {
                FileInfo item = row.getItem();
                SqlSession session1 = MybatisSqlSession.getSession();
                FileInfoDao fileInfoDao = session1.getMapper(FileInfoDao.class);
                fileInfoDao.delete(item);
                table.getItems().remove(row.getItem());
                session1.close();
                table.refresh();
            });


            newButton.setOnAction(event -> {
                ControllerContainer.add(GUI.NEW_STAGE_NAME,GUI.NEW_STAGE_PATH,new NewController());
                ((Stage) ControllerContainer.getStageByName(GUI.NEW_STAGE_NAME)).show();
                table.refresh();
            });

            attribute.setOnAction(event -> {
                FileInfo item = row.getItem();
                System.out.println(" hhh "+item);
                attribute(item);
                table.refresh();
            });

            contextMenu.getItems().addAll(newFile,delete,attribute,fresh);

            row.contextMenuProperty().bind(
                    Bindings.when(Bindings.isNotNull(row.itemProperty()))
                            .then(contextMenu)
                            .otherwise((ContextMenu)null));
            return row;
        });




        table.setItems(observableList);



        session.close();
    }



    public void attribute(FileInfo item){
        ControllerContainer.add(GUI.PROPERTY_STAGE_NAME,GUI.PROPERTY_STAGE_PATH,new PropertyController(item));
        ((PropertyController) ControllerContainer.getControllerByName(GUI.PROPERTY_STAGE_NAME)).refresh();
        ((Stage) ControllerContainer.getStageByName(GUI.PROPERTY_STAGE_NAME)).show();
        table.refresh();
    }


    @Override
    public void refresh() {
        table.refresh();
    }

    public List<FileInfo> getData() {
        return data;
    }

    public void addData(FileInfo fileInfo) {
        data.add(fileInfo);
    }
}
