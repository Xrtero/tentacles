package pers.xrtero.tentacles.controller.impl;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pers.xrtero.tentacles.controller.Controller;
import pers.xrtero.tentacles.entity.FileInfo;
import pers.xrtero.tentacles.entity.Packet;
import pers.xrtero.tentacles.enums.FileStatusEnum.FileStatusEnum;
import pers.xrtero.tentacles.service.DownloadThread;
import pers.xrtero.tentacles.service.Downloader;
import pers.xrtero.tentacles.utils.AlertUtils;
import pers.xrtero.tentacles.utils.ControllerContainer;
import pers.xrtero.tentacles.utils.GUI;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


/**
 * 下载UI控制器
 * @author Xrtero
 */
public class DownloadingController implements Controller {

    private Downloader downloader = null;
    private List<Packet> packets = null;
    private Long startTime = null;

    @FXML
    ProgressBar progressBar;

    @FXML
    private Label status;
    @FXML
    private Label size;
    @FXML
    private Label downloaded;
    @FXML
    private Label resume;
    @FXML
    private Label speed;

    @FXML
    private Button stopButton;
    @FXML
    private TableView threadTable;
    @FXML
    private TableColumn threadId;
    @FXML
    private TableColumn threadDownloaded;


    public DownloadingController(Downloader downloader) {
        this.downloader = downloader;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        progressBar.setProgress(0);

        try {
            FileInfo fileInfo = downloader.getFileInfo();
            Platform.runLater(()->{
                List<FileInfo> data = ((MainController) ControllerContainer.getControllerByName(GUI.MAIN_STAGE_NAME)).getData();
                data.add(fileInfo);
            });

            //获取每个线程中包的信息
            Thread[] threads = downloader.getThreads();
            DownloadThread[] downloadThreads = downloader.getDownloadThreads();
            List<Packet> packets = new ArrayList<>();
            for (DownloadThread downloadThread : downloadThreads) {
                packets.add(downloadThread.getPacket());
            }

            //界面基本信息初始化
            status.setText(fileInfo.getFileStatus().getValue());
            size.setText(downloader.format(downloader.getContentSize().get()));
            resume.setText(fileInfo.getResume()?"支持":"不支持");



            //事件以及属性绑定
            ObservableList observableList = FXCollections.observableArrayList(packets);
            threadId.setCellValueFactory(
                    new PropertyValueFactory<>("id")
            );
            threadDownloaded.setCellValueFactory(
                    new PropertyValueFactory<>("downloaded")
            );
            threadTable.setItems(observableList);

            //暂停
            stopButton.setOnAction(event -> stopped());
            //当文件不支持断点续传时，暂停按钮不可见
            if(!fileInfo.getResume()){
                stopButton.setVisible(false);
            }



            //开始下载
            startTime  = System.currentTimeMillis();
            downloader.work();
            Thread monitor = new Thread(()->{
                try {
                    while(true){
                        Thread.sleep(1000);
                        //刷新页面
                        refresh();
                        //暂停
                        if(fileInfo.getFileStatus().compareTo(FileStatusEnum.pause)==0){
                            ControllerContainer.getControllerByName(GUI.MAIN_STAGE_NAME).refresh();
                            break;
                        }
                        //下载完成
                        if(downloader.isDownloaded()){
                            stopButton.setDisable(true);
                            ControllerContainer.getControllerByName(GUI.MAIN_STAGE_NAME).refresh();
                            succeed();
                            break;
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } );
            monitor.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public void succeed(){
        Platform.runLater(()->{
            AlertUtils.informationBuild("下载完成",downloader.getFileInfo().getFileName()+"下载完成!");
            downloader.saveOnSuccess();
            ((Stage) ControllerContainer.getStageByName(GUI.DOWNLOADING_STAGE_NAME)).close();
            ControllerContainer.remove(GUI.DOWNLOADING_STAGE_NAME);
        });
    }

    public void stopped(){
        downloader.stopThreadGroup();
        downloader.save();
    }
    @Override
    public void refresh() {
        Platform.runLater(()->{
            progressBar.setProgress(downloader.getDownloadedProgress());
            ((DownloadingController) ControllerContainer.getControllerByName(GUI.DOWNLOADING_STAGE_NAME)).getThreadTable().refresh();
            downloaded.setText(downloader.getDownloadedSize());
            speed.setText(downloader.getSpeed(startTime));
        });

    }



    public Downloader getDownloader() {
        return downloader;
    }

    public List<Packet> getPackets() {
        return packets;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public Label getStatus() {
        return status;
    }

    public Label getSize() {
        return size;
    }

    public Label getDownloaded() {
        return downloaded;
    }

    public Label getResume() {
        return resume;
    }

    public Button getStopButton() {
        return stopButton;
    }

    public TableView getThreadTable() {
        return threadTable;
    }

    public TableColumn getThreadId() {
        return threadId;
    }

    public TableColumn getThreadDownloaded() {
        return threadDownloaded;
    }


}
