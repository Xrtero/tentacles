package pers.xrtero.tentacles.controller.impl;


import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import pers.xrtero.tentacles.controller.Controller;
import pers.xrtero.tentacles.utils.AlertUtils;
import pers.xrtero.tentacles.utils.Config;
import pers.xrtero.tentacles.utils.GUI;

import javax.xml.transform.sax.TemplatesHandler;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * 配置UI
 * @author Xrtero
 */
public class SettingsController implements Controller {

    @FXML private Button chooserFileButton;
    @FXML private TextField threadSize;
    @FXML private TextField localPath;

    private String path;
    private String threadSizeString;


    public SettingsController() {
        this.path = Config.localFileSavePath;
        this.threadSizeString = Config.threadSize.toString();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {



        //为按钮绑定事件
        chooserFileButton.setOnAction(event -> {
            DirectoryChooser fileChooser = new DirectoryChooser();
            fileChooser.setTitle("路径");
            File file = fileChooser.showDialog(new Stage());
            if(file!=null){
                localPath.setText(file.toString());
            }
        });

        localPath.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!oldValue.equals(newValue)){
                File file = new File(newValue);
                if(!file.isDirectory()){
                    localPath.setText(oldValue);
                    Config.setLocalFileSavePath(oldValue);
                    AlertUtils.warmingBuild("错误","这不是一个目录！");
                }else{
                    Config.setLocalFileSavePath(newValue);
                    localPath.setText(newValue);
                }
            }

        });

        threadSize.setOnAction(event -> {
            String text = threadSize.getText();
            try {
                Integer size = Integer.valueOf(text);
                if(size>=1&&size<=128){
                    Config.setThreadSize(size);
                    threadSize.setText(size.toString());
                    threadSizeString = threadSize.getText();
                }
            } catch (NumberFormatException e) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        AlertUtils.warmingBuild("输入错误","请输入一个数字！");
                    }
                });
                e.printStackTrace();
            }
        });


    }


    @Override
    public void refresh() {
        Platform.runLater(()->{
            threadSize.setText(threadSizeString);
            localPath.setText(path);
        });

    }
}

