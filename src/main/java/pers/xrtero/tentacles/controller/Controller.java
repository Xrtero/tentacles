package pers.xrtero.tentacles.controller;

import javafx.fxml.Initializable;

/**
 * 控制器接口
 *  @author Xrtero
 */
public interface Controller extends Initializable {

    /**
     * 界面更新
     */
    void refresh();


}
