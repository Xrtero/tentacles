package pers.xrtero.tentacles.service;

import pers.xrtero.tentacles.entity.Packet;
import pers.xrtero.tentacles.utils.Config;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 下载任务子线程,用来向服务器请求一节数据并下载存储
 * @author Xrtero
 */
public class DownloadThread implements Runnable{
    private Packet packet;
    private String fileAbsolutePath;
    private URL url;
    private AtomicInteger aliveThreads ;
    private  AtomicLong downloaded;
    private  AtomicLong contentSize;

    public DownloadThread() {
    }

    public DownloadThread(Packet packet, String fileAbsolutePath, URL url, AtomicInteger aliveThreads, AtomicLong downloaded, AtomicLong contentSize) {
        this.packet = packet;
        this.fileAbsolutePath = fileAbsolutePath;
        this.url = url;
        this.aliveThreads = aliveThreads;
        this.downloaded = downloaded;
        this.contentSize = contentSize;
    }

    @Override
    public void run() {
        InputStream in = null;
        URLConnection conn = null;
        try(RandomAccessFile raf =  new RandomAccessFile(fileAbsolutePath,"rwd"))
        {
            conn = url.openConnection();
            conn.setRequestProperty("range","bytes="+packet.getStart()+"-"+packet.getEnd());
            conn.setRequestProperty("Content-Disposition","attachment");
            conn.setDoOutput(true);
            conn.setConnectTimeout(Config.timeOut);
            conn.setReadTimeout(Config.timeOut);
            conn.connect();
            in =  conn.getInputStream();
            byte[] bytes = null;
            if(conn.getContentLength()<1024){
                bytes = new byte[conn.getContentLength()];
            }
            bytes = new byte[1024];
            raf.seek(packet.getStart());
            int readSize = 0;
            while((readSize=in.read(bytes))>0){
                downloaded.addAndGet(readSize);
                raf.write(bytes,0,readSize);
                packet.setDownloaded(packet.getDownloaded()+readSize);
                if(Thread.currentThread().isInterrupted()){
                    //向数据库写入数据，并退出此线程

                    break;
                }
            }
        }
        catch (SocketTimeoutException e){

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }finally
        {
            aliveThreads.decrementAndGet();
            if(in!=null){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
    public Packet getPacket() {
        return packet;
    }


    public void setPacket(Packet packet) {
        this.packet = packet;
    }

    public String getFileAbsolutePath() {
        return fileAbsolutePath;
    }

    public void setFileAbsolutePath(String fileAbsolutePath) {
        this.fileAbsolutePath = fileAbsolutePath;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public AtomicInteger getAliveThreads() {
        return aliveThreads;
    }

    public void setAliveThreads(AtomicInteger aliveThreads) {
        this.aliveThreads = aliveThreads;
    }

    public AtomicLong getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(AtomicLong downloaded) {
        this.downloaded = downloaded;
    }

    public AtomicLong getContentSize() {
        return contentSize;
    }

    public void setContentSize(AtomicLong contentSize) {
        this.contentSize = contentSize;
    }
}