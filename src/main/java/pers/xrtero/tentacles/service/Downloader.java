package pers.xrtero.tentacles.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.tika.metadata.HttpHeaders;
import pers.xrtero.tentacles.dao.FileInfoDao;
import pers.xrtero.tentacles.entity.FileInfo;
import pers.xrtero.tentacles.entity.Packet;
import pers.xrtero.tentacles.enums.FileStatusEnum.FileStatusEnum;
import pers.xrtero.tentacles.exception.URLException;
import pers.xrtero.tentacles.utils.Config;
import pers.xrtero.tentacles.utils.FileTypeHandler;
import pers.xrtero.tentacles.utils.MybatisSqlSession;
import pers.xrtero.tentacles.dao.SubThreadDao;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 用来执行下载任务
 * @author Xrtero
 */

public class Downloader {
    private LinkedHashMap<Integer, Packet> packages = new LinkedHashMap<>();
    private Integer TimeOut = Config.timeOut;
    FileInfo fileInfo = null;
    private  AtomicInteger aliveThreads = new AtomicInteger(Config.threadSize);
    private AtomicLong downloaded = new AtomicLong(0);
    private  AtomicLong contentSize = new AtomicLong(0);
    private Object waiting = new Object();
    private DownloadThread[] downloadThreads;
    private Thread[] threads;
    private HttpURLConnection conn = null;
    private Boolean resume = false;
    private ThreadGroup threadGroup;

    public Downloader(HttpURLConnection conn) throws URLException,IOException {
        downloadThreads = new DownloadThread[Config.threadSize];
        threads = new Thread[Config.threadSize];
        this.conn = conn;
        initialize();
        threadGroup = new ThreadGroup(fileInfo.getFileName());
    }

    public Downloader(Integer id) throws InstantiationException{
        //开启session
        SqlSession fileInfoSession = MybatisSqlSession.getSession();
        FileInfoDao fileInfoDao = fileInfoSession.getMapper(FileInfoDao.class);
        FileInfo fileInfo = fileInfoDao.getById(id);
        resume = true;
        this.fileInfo = fileInfo;
        fileInfo.setFileStatus(FileStatusEnum.downloading);
        if(!fileInfo.getResume()){
            throw new InstantiationException("不支持断点续传");
        }
        SqlSession subThreadsSession = MybatisSqlSession.getSession();
        SubThreadDao subThreadDao = subThreadsSession.getMapper(SubThreadDao.class);
        List<Packet> packets = subThreadDao.queryById(fileInfo.getId());
        subThreadDao.delete(fileInfo.getId());
        URL url = null;
        URLConnection conn = null;
        try {
            url = new URL(fileInfo.getUrl());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //获得线程数
        aliveThreads.set(packets.size());
        //计算还需要下载的大小

        for (int i = 0; i < packets.size(); i++) {
            packets.get(i).setId(i+1);
            packets.get(i).setStart(packets.get(i).getStart()+packets.get(i).getDownloaded());
            packets.get(i).setSize(packets.get(i).getEnd() - packets.get(i).getStart());
            contentSize.addAndGet(packets.get(i).getSize()+1);
        }

        threadGroup = new ThreadGroup(fileInfo.getFileName()){
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                stopThreadGroup();
                if(fileInfo.getResume()){
                    save();
                }
            }
        };
        //构建子线程
        threads = new Thread[aliveThreads.get()];
        downloadThreads = new DownloadThread[aliveThreads.get()];
        for (int i = 0; i < packets.size(); i++) {
            downloadThreads[i] = new DownloadThread();
            downloadThreads[i].setPacket(packets.get(i));
            downloadThreads[i].setFileAbsolutePath(fileInfo.getFileAbsolutePath());
            downloadThreads[i].setUrl(url);
            downloadThreads[i].setAliveThreads(aliveThreads);
            downloadThreads[i].setDownloaded(downloaded);
            downloadThreads[i].setContentSize(contentSize);
            threads[i] = new Thread(threadGroup,downloadThreads[i]);
        }

    }

    public void initialize() throws IOException{
        //获取当前文件id
        SqlSession session = MybatisSqlSession.getSession();
        FileInfoDao mapper = session.getMapper(FileInfoDao.class);

        String fileName = null;
        Long fileSize = 0L;
        LocalDate fileCreationTime = null;
        Boolean resume = true;
        //从header 获取数据
        Map<String, List<String>> headerFields = conn.getHeaderFields();
        //封装数据
        //文件大小
        fileSize = conn.getContentLengthLong();
        //文件名
        fileName = FileTypeHandler.parser(headerFields.get(HttpHeaders.CONTENT_TYPE).get(0), conn.getURL().getFile());
        fileCreationTime = LocalDate.now();
        if (fileName == null) {
            try {
                throw new IOException("无法解析文件");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // 判断是否支持断点续传
        String acceptRange = conn.getHeaderField("Accept-Range") == null ? new String() : conn.getHeaderField("Accept-Range");
        String acceptRanges = conn.getHeaderField("Accept-Ranges") == null ? new String() : conn.getHeaderField("Accept-Ranges");
        if (acceptRanges.equals("none") || acceptRange.equals("none")) {
            resume = false;
        }

        this.contentSize.addAndGet(fileSize);
        Integer maxId = mapper.getMaxId();
        fileInfo =
                new FileInfo(maxId==null?1:maxId+1,
                        fileName,
                        fileSize,
                        FileStatusEnum.downloading,Date.valueOf(fileCreationTime),
                        Config.localFileSavePath+fileName,
                        resume,
                        conn.getURL().toString());

        Long block = fileInfo.getFileSize()/(Config.threadSize-1);

        // 分包
        for (int i = 1; i < Config.threadSize; i++) {
            packages.put(i,new Packet(i,block,block*(i-1),block*i-1,0L));
        }
        Long last = fileInfo.getFileSize()%(Config.threadSize-1);
        packages.put(Config.threadSize,new Packet(Config.threadSize,last,block*(Config.threadSize-1), fileInfo.getFileSize(),0L));


        //创建文件
        File file = new File(fileInfo.getFileAbsolutePath());
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        threadGroup = new ThreadGroup(fileInfo.getFileName()){
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                stopThreadGroup();
                if(fileInfo.getResume()){
                    save();
                }
            }
        };

        packages.forEach((key,value) -> {
            downloadThreads[key-1] = new DownloadThread(value, fileInfo.getFileAbsolutePath(),conn.getURL(),aliveThreads,downloaded,contentSize);
            threads[key-1] = new Thread(threadGroup,downloadThreads[key-1]);
        });

        session.close();
    }


    /**
     * @Description 启用线程开始下载
     */
    public void work(){

        for (Thread thread : threads) {
            thread.start();
        }
}

    /**
     * @Description  断点续传
     */
    public void resume(){
        work();
    }

    /**
     *  暂停线程组
     */
    public void stopThreadGroup(){
        threadGroup.interrupt();
    }

    /**
     * 将信息写入数据库
     */
    public void save(){

        fileInfo.setDownloaded(this.getFileDownloaded());
        fileInfo.setFileStatus(FileStatusEnum.pause);

        //开启session
        SqlSession fileInfoSession = MybatisSqlSession.getSession();
        SqlSession subThreadSession = MybatisSqlSession.getSession();
        FileInfoDao fileInfoDao = fileInfoSession.getMapper(FileInfoDao.class);
        SubThreadDao subThreadDao = subThreadSession.getMapper(SubThreadDao.class);


        //存储文件信息
        fileInfoDao.save(fileInfo);


        //存储线程
        for (DownloadThread thread : downloadThreads) {
            if(thread.getPacket().getDownloaded()
                    .compareTo(thread.getPacket().getEnd()-thread.getPacket().getStart())==-1){
                subThreadDao.save(thread.getPacket(),fileInfo.getId());
            }
        }

        //关闭事务
        fileInfoSession.close();
        subThreadSession.close();
    }


    public void saveOnSuccess(){
        fileInfo.setDownloaded(fileInfo.getFileSize());
        fileInfo.setFileStatus(FileStatusEnum.finished);
        SqlSession fileInfoSession = MybatisSqlSession.getSession();
        FileInfoDao fileInfoDao = fileInfoSession.getMapper(FileInfoDao.class);
        fileInfoDao.updateStatus(fileInfo);

        fileInfoSession.close();
    }

    /**
     * 获取当前下载速度
     * @param start 开始下载时间
     * @return 返回一个下载速度的字符串
     */
    public String getSpeed(Long start){
        long now = System.currentTimeMillis();
        long spend = (now - start)/1000;
        long speed = this.downloaded.get() / spend;
        if(speed >= 1048576L){
            return  String.format("%.2f",speed / 1048576.0F) + "Mib";
        }else if(speed >= 1024L) {
            return String.format("%.2f",speed / 1024.0F) + "Kib";
        }else {
            return speed + "B";
        }
    }

    /**
     * 计算后缀
     * @param size
     * @return
     */
    public static String format(Long size){
        if (size >= 1073741824L)
            return  String.format("%.2f",size / 1073741824.0F) + " GiB";
        if (size >= 1048576L)
            return  String.format("%.2f",size / 1048576.0F) + " MiB";
        if (size >= 1024)
            return  String.format("%.2f",size / 1024.0F) + " KiB";
        return size + " B";
    }


    public boolean isDownloaded(){
        if(downloaded.get()==contentSize.get()&&aliveThreads.get()==0){
            return true;
        }
        return false;
    }

    public String getDownloadedSize(){
        return format(this.downloaded.get());
    }
    public Long getFileDownloaded(){
        return this.downloaded.get();
    }

    public double getDownloadedProgress(){
        return this.downloaded.get() / this.contentSize.doubleValue();
    }

    public AtomicInteger getAliveThreads() {
        return aliveThreads;
    }

    public AtomicLong getContentSize() {
        return contentSize;
    }

    public FileInfo getFileInfo() {
        return fileInfo;
    }


    public DownloadThread[] getDownloadThreads() {
        return downloadThreads;
    }

    public Thread[] getThreads() {
        return threads;
    }

    public static void main(String[] args) throws MalformedURLException, FileNotFoundException {
        try {
            new Downloader((HttpURLConnection) new URL("https://d1.music.126.net/dmusic/cloudmusicsetup2.7.5.198554.exe").openConnection()).work();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ThreadGroup getThreadGroup() {
        return threadGroup;
    }
}