package pers.xrtero.tentacles.boot;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import pers.xrtero.tentacles.controller.Controller;
import pers.xrtero.tentacles.controller.impl.MainController;
import pers.xrtero.tentacles.utils.Config;
import pers.xrtero.tentacles.utils.ControllerContainer;
import pers.xrtero.tentacles.utils.GUI;

import java.io.IOException;

/**
 * 主启动器
 * @author Xrtero
 */
public class JavaFxApplication extends Application {

    public static void main(String[] args) {
        Application.launch(JavaFxApplication.class,args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        ControllerContainer.add(GUI.MAIN_STAGE_NAME,GUI.MAIN_STAGE_PATH,new MainController());
        Stage stage = ControllerContainer.getStageByName(GUI.MAIN_STAGE_NAME);

        stage.getScene().getStylesheets().add("/css/style.css");
        stage.setTitle("Tentacles");
        stage.setResizable(false);
        stage.getIcons().add(new Image(JavaFxApplication.class.getResourceAsStream("/icon/mascot.png")));

        //程序关闭时操作
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                //将配置写入文件
                Config.saveConfig();
                System.exit(0);
            }
        });

        primaryStage = stage;
        primaryStage.show();
    }



}
