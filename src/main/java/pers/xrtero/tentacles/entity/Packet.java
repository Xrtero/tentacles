package pers.xrtero.tentacles.entity;

/**
 * packet entity
 * @author Xrtero
 */
public class Packet {
    private Integer id;
    private Long size;
    private Long start;
    private Long end;
    private Long downloaded;

    public Packet(Integer id, Long size, Long start, Long end, Long downloaded) {
        this.id = id;
        this.size = size;
        this.start = start;
        this.end = end;
        this.downloaded = downloaded;
    }

    public Packet(Long start, Long end, Long downloaded) {
        this.start = start;
        this.end = end;
        this.downloaded = downloaded;
    }

    @Override
    public String toString() {
        return "Packet{" +
                "id=" + id +
                ", size=" + size +
                ", start=" + start +
                ", end=" + end +
                ", downloaded=" + downloaded +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Long downloaded) {
        this.downloaded = downloaded;
    }
}
