package pers.xrtero.tentacles.entity;

import pers.xrtero.tentacles.enums.FileStatusEnum.FileStatusEnum;

import java.sql.Date;

/**
 * fileInfo entity
 * @author Xrtero
 */
public class FileInfo {

    private Integer id;
    private String fileName;
    private Long fileSize;
    private Long downloaded;
    private FileStatusEnum fileStatus;
    private Date fileCreationTime;
    private String fileAbsolutePath;
    private Boolean resume;
    private String url;

    public FileInfo(Integer id, String fileName, Long fileSize, Long downloaded, FileStatusEnum fileStatus, Date fileCreationTime, String fileAbsolutePath, Boolean resume, String url) {
        this.id = id;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.downloaded = downloaded;
        this.fileStatus = fileStatus;
        this.fileCreationTime = fileCreationTime;
        this.fileAbsolutePath = fileAbsolutePath;
        this.resume = resume;
        this.url = url;
    }

    public FileInfo(Integer id, String fileName, Long fileSize, FileStatusEnum fileStatus, Date fileCreationTime, String fileAbsolutePath, Boolean resume, String url) {
        this.id = id;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileStatus = fileStatus;
        this.fileCreationTime = fileCreationTime;
        this.fileAbsolutePath = fileAbsolutePath;
        this.resume = resume;
        this.url = url;
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                ", fileStatus=" + fileStatus +
                ", fileCreationTime=" + fileCreationTime +
                ", fileAbsolutePath='" + fileAbsolutePath + '\'' +
                ", resume=" + resume +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public FileStatusEnum getFileStatus() {
        return fileStatus;
    }

    public void setFileStatus(FileStatusEnum fileStatusEnum) {
        this.fileStatus = fileStatusEnum;
    }

    public Date getFileCreationTime() {
        return fileCreationTime;
    }

    public void setFileCreationTime(Date fileCreationTime) {
        this.fileCreationTime = fileCreationTime;
    }

    public String getFileAbsolutePath() {
        return fileAbsolutePath;
    }

    public void setFileAbsolutePath(String fileAbsolutePath) {
        this.fileAbsolutePath = fileAbsolutePath;
    }

    public Boolean getResume() {
        return resume;
    }

    public void setResume(Boolean resume) {
        this.resume = resume;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Long downloaded) {
        this.downloaded = downloaded;
    }
}
