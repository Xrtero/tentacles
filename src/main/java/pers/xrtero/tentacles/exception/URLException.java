package pers.xrtero.tentacles.exception;


import pers.xrtero.tentacles.enums.ErrorCode;

/**
 * URL Exception
 * @author Xrtero
 */
public class URLException extends RuntimeException {


    private Enum<ErrorCode> errorCodeEnum;


    public URLException(Enum errorCodeEnum){
        this.errorCodeEnum = errorCodeEnum;
    }


    @Override
    public String toString() {
        return "URLException"+errorCodeEnum.toString();
    }
}
