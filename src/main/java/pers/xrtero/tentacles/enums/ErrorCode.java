package pers.xrtero.tentacles.enums;

/**
 * URL 异常枚举
 */
public enum ErrorCode {

    URL_ERROR("777","不支持此URL");


    private String code;
    private String description;

    ErrorCode(String code, String description) {
        this.code = code;
        this.description = description;
    }


    @Override
    public String toString() {
        return "code:'" + code +"    "+" description:'" + description ;
    }
}
