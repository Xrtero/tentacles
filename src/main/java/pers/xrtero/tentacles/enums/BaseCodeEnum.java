package pers.xrtero.tentacles.enums;

/**
 * 枚举接口
 * @author Xrtero
 */
public interface BaseCodeEnum {
    String getValue();
    String getCode();
}
