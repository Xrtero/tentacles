package pers.xrtero.tentacles.enums.FileStatusEnum.handler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import pers.xrtero.tentacles.enums.BaseCodeEnum;
import pers.xrtero.tentacles.enums.FileStatusEnum.FileStatusEnum;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 文件状态枚举处理器
 * @author Xrtero
 */
public class FileStatusEnumTypeHandler extends BaseTypeHandler<FileStatusEnum> {
    /**
     * 定义设置参数时，该如何把Java类型的参数转换为对应的数据库类型
     * @param ps
     * @param i
     * @param parameter
     * @param jdbcType
     * @throws SQLException
     */
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, FileStatusEnum parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i,parameter.getCode());
    }
    /**
     * @Description 用于定义通过字段名称获取字段数据时，如何把数据库类型转换为对应的Java类型
     * @param rs
     * @param columnName
     * @return
     * @throws SQLException
     */
    @Override
    public FileStatusEnum getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String code = rs.getString(columnName);
        return rs.wasNull()?null:codeOf(FileStatusEnum.class,code);
    }

    /**
     * 用于定义通过字段索引获取字段数据时，如何把数据库类型转换为对应的Java类型
     * @param rs
     * @param columnIndex
     * @return
     * @throws SQLException
     */
    @Override
    public FileStatusEnum getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        String code = rs.getString(columnIndex);
        return rs.wasNull()?null:codeOf(FileStatusEnum.class,code);
    }

    /**
     * 用定义调用存储过程后，如何把数据库类型转换为对应的Java类型
     * @param cs
     * @param columnIndex
     * @return
     * @throws SQLException
     */
    @Override
    public FileStatusEnum getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        String code = cs.getString(columnIndex);
        return cs.wasNull()?null:codeOf(FileStatusEnum.class,code);
    }

    public static <E extends  Enum<?> & BaseCodeEnum> E codeOf(Class<E> enumClass,String code){
        E[] enumConstants = enumClass.getEnumConstants();
        for (E e : enumConstants) {
            if(e.getCode().equals(code)){
                return e;
            }
        }
        return null;

    }

}
