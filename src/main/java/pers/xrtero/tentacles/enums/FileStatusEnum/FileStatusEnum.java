package pers.xrtero.tentacles.enums.FileStatusEnum;

import pers.xrtero.tentacles.enums.BaseCodeEnum;

/**
 * 文件状态枚举
 * @author Xrtero
 */
public enum FileStatusEnum implements BaseCodeEnum {

    /**
     * 完成
     */
    finished("finished","完成"),

    /**
     * 暂停或未开始
     */
    pause("pause","暂停"),

    /**
     * 不支持此URL或发生未知错误
     */
    error("error","错误"),
    /**
     * 下载中
     */
    downloading("downloading","下载中");

    private String value;
    private String code;

    FileStatusEnum(String code,String value) {
        this.value = value;
        this.code = code;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String toString() {
        return value;
    }
}
