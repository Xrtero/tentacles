package pers.xrtero.tentacles.dao;

import org.apache.ibatis.annotations.Param;
import pers.xrtero.tentacles.entity.Packet;

import java.util.List;

/**
 * sub_thread_packet 表 dao接口
 * @author Xrtero
 */
public interface SubThreadDao {
    public void save(@Param("packet")Packet packet,@Param("fileId")Integer id);
    public void saveList();
    public List<Packet> queryById(Integer id);
    public void delete(Integer id);

}
