package pers.xrtero.tentacles.dao;

import pers.xrtero.tentacles.entity.FileInfo;
import pers.xrtero.tentacles.enums.FileStatusEnum.FileStatusEnum;

import java.util.Date;
import java.util.List;

/**
 * file_info 表的dao接口
 * @author Xrtero
 */
public interface FileInfoDao {

    public List<FileInfo> getAll();
    public FileInfo getById(Integer id);
    public Integer getMaxId();


    public void save(FileInfo fileInfo);

    public void updateName(FileInfo fileInfo);
    public void updateStatus(FileInfo fileInfo);

    public void delete(FileInfo fileInfo);

}
