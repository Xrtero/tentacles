import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.activation.MimeTypeParseException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class TikaTest {
   private  static URL url;

    static {
        try {
            url = new URL("https://d1.music.126.net/dmusic/cloudmusicsetup2.7.5.198554.exe");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void StreamAutoDetectTest() throws IOException, MimeTypeException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("User-Agent","Mozilla/4.0(compatible;MSIE 5.0;Windows NT;DigExt)");
        MimeTypes all = MimeTypes.getDefaultMimeTypes();
        MimeType mimeType = all.forName(con.getContentType());
        System.out.println(con.getContentType());
        System.out.println(mimeType.getExtension());
        System.out.println(mimeType.getExtensions());

    }

    @Test
    public void TimeTest(){
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")));
    }


}
