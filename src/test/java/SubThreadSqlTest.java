import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import pers.xrtero.tentacles.dao.SubThreadDao;
import pers.xrtero.tentacles.entity.Packet;
import pers.xrtero.tentacles.utils.MybatisSqlSession;

import java.util.List;

public class SubThreadSqlTest {

    SqlSession session = MybatisSqlSession.getSession();
    SubThreadDao subThreadDao = session.getMapper(SubThreadDao.class);
    @Test
    public void query(){


        List<Packet> packets = subThreadDao.queryById(1);
        for (Packet packet : packets) {
            System.out.println(packet);
        }

        session.close();

    }

    @Test
    public void save(){

        subThreadDao.save(new Packet(1,1L,1L,1L,11111L),10);

        session.close();
    }

}
