import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import pers.xrtero.tentacles.dao.FileInfoDao;
import pers.xrtero.tentacles.entity.FileInfo;
import pers.xrtero.tentacles.enums.FileStatusEnum.FileStatusEnum;
import pers.xrtero.tentacles.utils.MybatisSqlSession;

import java.sql.Date;

public class FileInfoSqlTest {

    SqlSession session = MybatisSqlSession.getSession();
    FileInfoDao mapper = session.getMapper(FileInfoDao.class);
    private static FileInfo fileInfo = new FileInfo(3, "oiajsdofi.exe", 123123L, FileStatusEnum.finished, new Date(System.currentTimeMillis()), "C:/", true, "asodjfoiasjdfolaskdflk");

    @Test
    public void insertFileInfoTest(){

        mapper.save(fileInfo);
        session.close();

    }

    @Test
    public void updateFileInfoTest(){
        fileInfo.setFileName("hhhh.exe");
        mapper.updateName(fileInfo);
        session.close();

    }




}
