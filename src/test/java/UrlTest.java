import org.junit.Test;
import pers.xrtero.tentacles.service.Downloader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

public class UrlTest {
    private static URL url;
    static {
        try {
            url = new URL("https://d1.music.126.net/dmusic/cloudmusicsetup2.7.5.198554.exe");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private Downloader downloader;

    {
        try {
            downloader = new Downloader((HttpURLConnection) url.openConnection());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void headTest() throws IOException {
        Downloader downloader = new Downloader((HttpURLConnection) url.openConnection());


    }

    @Test
    public void contentBlockTest() throws MalformedURLException, FileNotFoundException {
        Downloader downloader = null;
        try {
            downloader = new Downloader((HttpURLConnection) url.openConnection());
        } catch (IOException e) {
            e.printStackTrace();
        }
        downloader.work();
    }

    /**
     * @Description 按range请求
     * @throws IOException
     */
    @Test
    public void aThreadDownloadTest() throws IOException {
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestProperty("Content-Disposition","attachment");
        conn.setRequestProperty("range","bytes=46693416-52530092");
        conn.getRequestProperties().forEach((key,value)->{
            System.out.println(key + " " + value);
        });

        conn.setConnectTimeout(5000);
        conn.setReadTimeout(5000);


        conn.connect();
        Map<String, List<String>> headerFields = conn.getHeaderFields();
        headerFields.forEach((key,value) -> {
            System.out.println(key + ":" + value);

        });
        InputStream inputStream = conn.getInputStream();
        byte[] bytes = new byte[1024];
        while (inputStream.read(bytes)>0){
            System.out.printf("", bytes);
        }
        inputStream.close();
    }

    @Test
    public void url(){
        String file = url.getFile();
        System.out.println(file);
    }


}
