import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import pers.xrtero.tentacles.dao.FileInfoDao;
import pers.xrtero.tentacles.entity.FileInfo;
import pers.xrtero.tentacles.utils.DruidDataSourcePool;
import pers.xrtero.tentacles.utils.MybatisSqlSession;

import java.sql.Connection;
import java.util.List;

public class DatabaseTest {


    @Test
    public void druidConnectionTest(){
        Connection connection = DruidDataSourcePool.getConnection();
        System.out.println(connection);
    }


    @Test
    public void SessionTest(){
        SqlSession session = MybatisSqlSession.getSession();
        FileInfoDao mapper = session.getMapper(FileInfoDao.class);
        List<FileInfo> all = mapper.getAll();
        for (FileInfo fileInfo : all) {
            System.out.println(fileInfo);
        }
        System.out.println(mapper);
    }



}

