#开发计划  
目前本项目还在开发中，存在着许多问题。
以下是目前规划的开发计划：  
v 1.0  
1.动态请求  
2.完成工具栏按钮与菜单栏  
3.优化所有请求线程  
#介绍  
一个基于java的多线程下载器
##技术栈  
  * UI： javaFX
  * 连接池：Druid
  * ORM：Mybatis
  * 数据库: mysql
#部署  
1.导入数据  
2.配置环境  
3.启动  
#项目结构  
![项目结构](Structure.png)  
#警告  
>本项目只能用于学习





